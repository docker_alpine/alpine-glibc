# alpine-glibc
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-glibc)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-glibc)



----------------------------------------
### x64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-glibc/x64)
### aarch64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-glibc/x64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64
* Appplication : [glibc](https://www.gnu.org/software/libc/)
    - The GNU C Library project provides the core libraries for the GNU system and GNU/Linux systems, as well as many other systems that use Linux as the kernel.



----------------------------------------
#### Run

```sh
docker run -i \
           -t \
           -rm \
           forumi0721/alpine-glibc:[ARCH_TAG]
```



----------------------------------------
#### Usage

* Library



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

